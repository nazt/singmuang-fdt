#!/usr/bin/python 
import cv2
import cv2.cv as cv
import sys

def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=3, minSize=(10, 10), flags = cv.CV_HAAR_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:,2:] += rects[:,:2]
    return rects

def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)

def draw_eyes(img, rects, color, face_rects):
    rects[:,2:] += rects[:,:2]
    for ix in range(len(rects)):
        cv2.rectangle(img, (face_rects[ix / 2][0] + rects[ix][0], face_rects[ix / 2][1] + rects[ix][1]), 
                            (face_rects[ix / 2][0] + rects[ix][2], face_rects[ix / 2][1] + rects[ix][3]), color, 2)


if __name__ == '__main__':
    if len(sys.argv) != 2:                                         ## Check for error in usage syntax

        print "Usage : python faces.py <image_file>" 

    else:
        target_image = "/home/ubuntu/singmuang/output/output_"+ sys.argv[1].split("/")[-1]
        img_mask_r = cv2.imread('/home/ubuntu/singmuang/gg/g.png', -1)
        img_mask = cv2.imread('/home/ubuntu/singmuang/gg/g.png', cv2.CV_LOAD_IMAGE_COLOR)
        img = cv2.imread(sys.argv[1], cv2.CV_LOAD_IMAGE_COLOR)  ## Read image file
        mask = cv2.imread('/home/ubuntu/singmuang/gg1.png', cv2.CV_LOAD_IMAGE_COLOR)
        if (img == None):                                      ## Check for invalid input
            print "Could not open or find the image"
        else:
            cascade = cv2.CascadeClassifier("/home/ubuntu/singmuang/haarcascade_frontalface_alt.xml")
            eyes_cascade = cv2.CascadeClassifier("/home/ubuntu/singmuang/haarcascade_eye_tree_eyeglasses.xml")

            gray = cv2.cvtColor(img, cv.CV_BGR2GRAY)
            gray = cv2.equalizeHist(gray)
            
            can_not_detect = 0

            try:
                rects = detect(gray, cascade)
                
                ## Extract face coordinates         
                x1 = rects[0][1]
                y1 = rects[0][0]
                x2 = rects[0][3]
                y2 = rects[0][2]
                
            except Exception, e:
                img = cv2.resize(img, (250, 250))
                x1 = 0
                y1 = 0
                x2 = 250
                y2 = 250
                can_not_detect = 1
            else:
                pass
            finally:
                pass


            face_width = (x2 - x1)
            face_height = (y2 - y1)

            face_width_offset = int((x2 - x1) * 0.5)
            face_height_offset = int((y2 - y1) * 0.5)

            mask_resize = cv2.resize(mask, (face_width + face_width_offset * 2 , face_height + face_height_offset * 2))

            ## Show face ROI
            vis = img.copy()
            draw_rects(vis, rects, (0, 255, 0))


            crop_face = img[abs(-face_height_offset * 1.2 + x1): abs(-face_height_offset * 1.2 + x1 + mask_resize.shape[0]), abs(-face_width_offset + y1):abs(-face_width_offset + y1 + mask_resize.shape[1])]

            if can_not_detect:
                crop_face = img

            crop_face = cv2.resize(crop_face, (250, 230))
            img_mask_g = img_mask.copy()
            img_mask[40:270, 200:450] = crop_face

            l_img = img_mask
            s_img = img_mask_r


            x_offset = 0
            y_offset = 0

            for c in range(0,3):
                l_img[y_offset:y_offset+s_img.shape[0], x_offset:x_offset+s_img.shape[1], c] =  s_img[:,:,c] * (s_img[:,:,3]/255.0) +  l_img[y_offset:y_offset+s_img.shape[0], x_offset:x_offset+s_img.shape[1], c] * (1.0 - s_img[:,:,3]/255.0)            

            l_img = cv2.cvtColor(l_img, cv2.COLOR_BGR2GRAY)

            cv2.imwrite(target_image, l_img)

